﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoClipController : MonoBehaviour {
	public float rightThreshold;
	public int ammo;


	private GameObject hider;
	private float fullValue;

	void Awake(){
		hider = transform.Find("ammocliphider").gameObject;
		fullValue = hider.transform.localScale.x;
	}

	void Update() {
		hider.transform.localScale = new Vector2((fullValue/3)*(3-ammo), hider.transform.localScale.y);
		hider.transform.localPosition = new Vector2(Map(0f, 1f, 0f, rightThreshold, ammo), 0);	
	}

	private float Map(float a1, float a2, float b1, float b2, float value) {
		return b1 + (value - a1) * (b2 - b1) / (a2 - a1);
	}
}
