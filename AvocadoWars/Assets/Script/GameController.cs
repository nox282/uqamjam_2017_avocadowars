﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndTurnListener : MonoBehaviour {

	private PlayerController player;
	private GameController gm;
	private TreeController tree;
	private UIController ui;


	public EndTurnListener(PlayerController _p, UIController _ui) {
		player = _p;
		player.TurnEnded += new EndTurnEventHandler(Ended);
		ui = _ui;
	}

	public EndTurnListener (PlayerController _p, TreeController _t) {
		player = _p;
		player.TurnEnded += new EndTurnEventHandler(Ended);
		tree = _t;
	}

    public EndTurnListener (PlayerController _p, GameController _gm) {
       	player = _p;
       	player.TurnEnded += new EndTurnEventHandler(Ended);
       	gm = _gm;
    }

    private void Ended(object sender, EventArgs e) {
       	if(gm != null) gm.changeTurn();
    	if(tree != null) tree.reload();
    	if(ui != null){
    		ui.updateTreeList();
    		ui.updateTurnLabel();
    		ui.focus(0);
    	}
    }

    public void Detach(){
       	player.TurnEnded -= new EndTurnEventHandler(Ended);
       	player = null;
    }
}

public class GameController : MonoBehaviour {

	public int winner;

	public PlayerController player1;
	public PlayerController player2;

	public GameObject MainTreeP1;
	public GameObject MainTreeP2;

	private EndTurnListener player1_EL;
	private EndTurnListener player2_EL;

	void Start(){
		DontDestroyOnLoad(transform.gameObject);
		
		PlayerController[] players = {player1, player2};
		int beginer = (int) UnityEngine.Random.Range(0, 1);

		players[beginer].gameObject.SetActive(true);

		player1_EL = new EndTurnListener(player1, this);
		player2_EL = new EndTurnListener(player2, this);
	}

	void Update(){
		if(MainTreeP1 == null)
			concede(player1);
		if(MainTreeP2 == null)
			concede(player2);
	}

	public void changeTurn(){


		if(player1.gameObject.activeSelf){
			swap(player1.gameObject, player2.gameObject);
		} else if(player2.gameObject.activeSelf) {
			swap(player2.gameObject, player1.gameObject);
		}
	}

	private void swap(GameObject p1, GameObject p2){
		p1.SetActive(false);
		p2.SetActive(true);
	}

	public void concede(PlayerController p){
		if(p == player1)
			winner = 2;
		else
			winner = 1;
		SceneManager.LoadScene ("End", LoadSceneMode.Single);
	}
}
