﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public Vector4 bouds;
	
	public bool fullscreen;
	public float fsSize;
	public Vector2 fsPosition;

	private Camera main;
	private Camera bg;

	void Start() {
		main = Camera.main;
		bg = GetComponentInChildren<Camera>();
	}

	void Update () {
		if(fullscreen){
			main.orthographicSize = fsSize;
			bg.orthographicSize = fsSize;
			transform.position = new Vector3(fsPosition.x, fsPosition.y, -10);
		} else
			checkBounds();
	}

	void checkBounds(){
		float height = Camera.main.orthographicSize;
		float width = height * Camera.main.aspect;

		Vector3 pos = transform.position;

		pos.x = pos.x - width  < bouds.x ? bouds.x + width  : pos.x;
		pos.y = pos.y + height > bouds.y ? bouds.y - height : pos.y;
		pos.x = pos.x + width  > bouds.z ? bouds.z - width  : pos.x;
		pos.y = pos.y - height < bouds.w ? bouds.w + height : pos.y;
		
		if(pos != transform.position)
			transform.position = pos;
	}
}
