﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void SeedCrashedEventHandler(object sender, EventArgs e);

public class SeedController : MonoBehaviour {
	public GameObject treePrefab;
	public int lifetime;
	public float damage;


	private int frameCount;
	private GameObject origin;
	private SpriteRenderer renderer;
	private Rigidbody2D rb;

	public event SeedCrashedEventHandler SeedCrashed;

	protected virtual void OnSeedCrashed(EventArgs e){
		if(SeedCrashed != null)
			SeedCrashed(this, e);
	}

	void Awake() {
		renderer = GetComponent<SpriteRenderer>();
		rb = GetComponent<Rigidbody2D>();
		frameCount = 0;
	}

	void Update(){
		frameCount++;
		if(frameCount > lifetime)
			Destroy(gameObject);
	}

	void OnCollisionEnter2D(Collision2D collision){
		OnSeedCrashed(EventArgs.Empty);
		if(collision.gameObject.CompareTag("ground")){
			renderer.color = new Color(0.5f, 0.5f, 0.5f, 1f);
			rb.angularDrag = 200f;
			rb.velocity = new Vector2(0, 0);

			
			sprout();
		}
	
		if(collision.gameObject.CompareTag("tree")){
			
			if(collision.gameObject != null){
				collision.gameObject.GetComponent<TreeController>().damage(damage);
				
			}				
		}
		Destroy(gameObject);
	}

	void sprout(){
		float offset = treePrefab.transform.localScale.y;
		Vector2 spawnPosition = new Vector2(transform.position.x, transform.position.y + offset);
		GameObject tree = Instantiate(treePrefab, spawnPosition, Quaternion.identity);
		tree.GetComponent<TreeController>().assign(origin);
	}

	public void assign(GameObject p){
		origin = p;
	}
}
