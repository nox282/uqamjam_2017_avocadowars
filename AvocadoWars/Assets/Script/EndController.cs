﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndController : MonoBehaviour {

	public Text endText;

	private GameController gm;

	void Start () {
		gm = FindObjectOfType<GameController>();

		if(gm.winner == 1)
			endText.text = "Player 1 Wins!";
		else
			endText.text = "Player 2 Wins!";

		Destroy(gm);
	}

	void Update(){
		if(Input.GetButtonDown("Fire1"))
			SceneManager.LoadScene ("Menu", LoadSceneMode.Single);
	}
}
