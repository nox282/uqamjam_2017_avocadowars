﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPBarController : MonoBehaviour {
	public float ratio;
	public float leftThreshold;

	private GameObject fullBar;
	private float fullvalue;

	void Start () {
		fullBar = transform.Find("Full").gameObject;
		fullvalue = fullBar.transform.localScale.x;
	}
	
	void Update () {
		fullBar.transform.localScale = new Vector2(fullvalue * ratio, fullBar.transform.localScale.y);
		fullBar.transform.localPosition = new Vector2(- Map(1f, 0f, 0f, leftThreshold, ratio) ,0);
	}

	private float Map(float a1, float a2, float b1, float b2, float value) {
		return b1 + (value - a1) * (b2 - b1) / (a2 - a1);
	}
}
