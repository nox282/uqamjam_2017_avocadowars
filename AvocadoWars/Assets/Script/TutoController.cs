﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutoController : MonoBehaviour {
	public Image container;
	public List<Sprite> images;
	int index = 0;
	
	void Start() {
		container.sprite = images[index];
	}

	void Update () {
		if(Input.GetButtonDown("Fire1")){
			index ++;
			if(index < images.Count)
				container.sprite = images[index];
			else
				SceneManager.LoadScene ("Menu", LoadSceneMode.Single);
		}
	}
}
