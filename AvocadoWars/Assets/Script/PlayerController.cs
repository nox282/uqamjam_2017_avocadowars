﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SeedCrashedListener{
	private SeedController seed;
	private PlayerController origin;

	public SeedCrashedListener(SeedController _s, PlayerController _p){
		seed = _s;
		seed.SeedCrashed += new SeedCrashedEventHandler(Crashed);
		origin = _p;
	}

	private void Crashed(object sender, EventArgs e) {
		if(seed != null)
			origin.OnCurrentSeedCrash(seed);
	}

	public void Detach(){
		seed.SeedCrashed -= new SeedCrashedEventHandler(Crashed);
		seed = null;
	}
}


public delegate void EndTurnEventHandler(object sender, EventArgs e);

public class PlayerController : MonoBehaviour {

	public Camera camera;
	public GameObject seedPrefab;
	public float seedOffset;

	public int currentTree;

	public GameObject currentSeed;
	private SeedCrashedListener currentSeed_EL;

	public float force;

	public List<TreeController> trees;
	
	public List<TreeController> Trees{
		get{return trees;}
	}

	public event EndTurnEventHandler TurnEnded;

	protected virtual void OnEndTurn(EventArgs e){
		if(TurnEnded != null)
			TurnEnded(this, e);
	}
	
	void Start() {
		trees[0].assign(gameObject);
	}

	void Update () {
		moveCamera();
		
		if(Input.GetButtonDown("Fire2"))
			loadSeed();

		if(Input.GetButtonDown("Fire1"))
			throwSeed();

		if(Input.GetButtonDown("Jump"))
			passTurn();

		if(Input.GetKeyDown(KeyCode.Escape)){
			Destroy(GameObject.Find("_GameMaster_"));
			SceneManager.LoadScene ("Menu", LoadSceneMode.Single);

		}
	}

	private void getCamera(){
		camera = Camera.main;
	}

	void moveCamera(){
		if(camera == null)
			getCamera();

		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");

		if(h != 0 || v != 0)
			camera.transform.Translate(new Vector2(h, v));
	}

	void throwSeed(){
		throwSeed(force);
	}

	private void throwSeed(float localForce){
		if(currentSeed == null)
			return;

		Vector2 target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
	
		Rigidbody2D rb = currentSeed.GetComponent<Rigidbody2D>();
		rb.simulated = true;

		target -= (Vector2) currentSeed.transform.position;
		rb.AddForce(target*localForce);
	}

	void loadSeed(){
		if(currentSeed != null)
			return;
		if(!trees[currentTree].fetchSeed())
			return;

		Vector2 targetPosition = new Vector2(
			trees[currentTree].transform.position.x,
			trees[currentTree].transform.position.y + transform.lossyScale.y/2 + seedOffset
		);

		currentSeed = Instantiate(seedPrefab, targetPosition, Quaternion.identity);
		currentSeed.GetComponent<SeedController>().assign(gameObject);
		currentSeed.GetComponent<Rigidbody2D>().simulated = false;

		currentSeed_EL = new SeedCrashedListener(currentSeed.GetComponent<SeedController>(), this);
	}

	public void passTurn(){
		OnEndTurn(EventArgs.Empty);
	}

	public void addTree(TreeController _t){
		trees.Add(_t);
	}

	public void OnCurrentSeedCrash(SeedController seed){
		StartCoroutine(wait(2));
		
		OnEndTurn(EventArgs.Empty);
		currentSeed_EL.Detach();
	}

	IEnumerator wait(float i) {
        yield return new WaitForSeconds(i);
    }
}
