﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TreesDropdown : MonoBehaviour {

	private Dropdown dp;
	
	void Start () {
		dp = GetComponent<Dropdown>();

		dp.onValueChanged.AddListener( delegate {
			OnMyValueChange(dp);
		});	
	}

	public void OnMyValueChange(Dropdown dd) {
		GetComponentInParent<UIController>().focus(dp.value);
	}
 
	void OnDestroy() {
  		dp.onValueChanged.RemoveAllListeners();
 	}
}
