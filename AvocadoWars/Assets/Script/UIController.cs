﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	public Dropdown drop;
	public Text playerTurnLabel;
	public Button passTurn;

	public PlayerController player1;
	public PlayerController player2;

	private PlayerController current;

	private EndTurnListener player1_EL;
	private EndTurnListener player2_EL;
	
	void Start () {
		player1_EL = new EndTurnListener(player1, this);
		player2_EL = new EndTurnListener(player2, this);

		passTurn.onClick.AddListener(delegate{
			current.passTurn();
		});

		updateTurnLabel();
		focus(0);
	}

	public void updateTurnLabel(){
		refreshCurrentTurn();
		playerTurnLabel.text = current.gameObject.name;
	}

	public void updateTreeList(){
		refreshCurrentTurn();
		if(current.Trees == null)
			return;

		drop.ClearOptions();
		List<string> options = new List<string>();

		foreach(TreeController t in current.Trees){
			options.Add(t.gameObject.name);
		}
	
		drop.AddOptions(options);
	}

	private void refreshCurrentTurn(){
		current = player1.gameObject.activeSelf ? player1 : player2;
	}

	public void focus(int index){
		if(index > current.Trees.Count)
			return;

		Vector3 target = new Vector3(
			current.Trees[index].transform.position.x,
			current.Trees[index].transform.position.y,
			Camera.main.transform.position.z	
		);

		Camera.main.transform.position = target;
		current.currentTree = index;
	}
}