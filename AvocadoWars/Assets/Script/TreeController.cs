﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeController : MonoBehaviour {
	public Color selected;
	public Color p1Color;
	public Color p2Color;

	public float maxHP;
	public int seedCapacity;
	public int turnToReload;

	private int turnCounter;

	public float deathOffset;

	private HPBarController healtbar;
	private AmmoClipController ammoclip;

	private PlayerController assigned;
	private Color assignedColor;

	private PlayerController player1;
	private PlayerController player2;	

	private EndTurnListener player1_EL;
	private EndTurnListener player2_EL;

	private Animator anim;

	private SpriteRenderer sr;

	private float hp;
	public float HP{get{return hp;}}

	private int seeds;
	public int Seeds{get{return seeds;}}

	void Awake() {
		hp = maxHP;
		seeds = 1;

		sr = GetComponent<SpriteRenderer>();

		healtbar = transform.Find("HealthBar").GetComponent<HPBarController>();
		ammoclip = transform.Find("AmmoClip").GetComponent<AmmoClipController>();

		updateAmmoClip();

		anim = GetComponent<Animator>();

		player1 = GameObject.Find("_GameMaster_").GetComponent<GameController>().player1;
		player2 = GameObject.Find("_GameMaster_").GetComponent<GameController>().player2;

		player1_EL = new EndTurnListener(player1, this);
		player2_EL = new EndTurnListener(player2, this);
	}
	
	void OnMouseDown(){
		assigned.currentTree = assigned.Trees.FindIndex(x => x.gameObject == this.gameObject);
	}

	private float getHPRatio(){
		return hp/maxHP;
	}

	private void updateAmmoClip(){
		ammoclip.ammo = seeds;
	}

	private void updateHPBar(){
		healtbar.ratio = getHPRatio();
	}

	public void assign(GameObject p){
		assigned = p.GetComponent<PlayerController>();
		if(!assigned.Trees.Find(x => x.gameObject.GetInstanceID() == this.gameObject.GetInstanceID())){
			assigned.addTree(this);
		}
	}

	public void reload(){
		turnCounter++;
		if(turnCounter > turnToReload){
			if(seeds < seedCapacity)
				seeds++;
			turnCounter = 0;
		}
		updateAmmoClip();
	}

	public bool fetchSeed(){
		if(seeds <= 0)
			return false;
		seeds--;
		updateAmmoClip();
		return true;
	}

	public void damage(float amount){
		hp = hp-amount < 0 ? 0 : hp-amount;
		updateHPBar();
		isDead();
	}

	private void isDead(){
		if(hp <= 0){
			healtbar.gameObject.SetActive(false);
			ammoclip.gameObject.SetActive(false);
			anim.SetTrigger("death");
		}
	}
	public void die(){
		assigned.Trees.Remove(this);
		anim.ResetTrigger("death");
		Destroy(gameObject);
	}
}
