﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MenuController : MonoBehaviour {

	public Button play;
	public Button tutorial;
	public Button quit;

	void Start(){
		play.onClick.AddListener(delegate{
			loadMain();
		});
		
		tutorial.onClick.AddListener(delegate{
			loadTutorial();
		});

		quit.onClick.AddListener(delegate{
			loadQuit();
		});
	}

	void loadMain(){
		SceneManager.LoadScene ("Main", LoadSceneMode.Single); 
	}

	void loadTutorial(){
		SceneManager.LoadScene ("Tutorial", LoadSceneMode.Single);
	}

	void loadQuit(){
		Application.Quit();
	}
}
